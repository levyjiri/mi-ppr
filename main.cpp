#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <list>
#include <stack>
#include <map>
#include <cmath>
#include <iomanip>
#include <fstream>
#include "mpi.h"
#include <stdlib.h>

using namespace std;

const string SEPARATOR = ",";
const int EMPTY_VALUE = 0;
const int BUF_LENGTH = 200;
const int MSG_LENGTH = 100; // vhodne pre max 6x6...
const int MOV_LENGTH = 50;
const int MSG_WORK_SENT = 0; // Message work send flag
const int MSG_SOLUTION_SENT = 1;
const int MSG_WORK_ASK = 2;
const int MSG_IS_END = 3;
const int MSG_IS_NOT_END = 4;

enum directionType {
    U, // up
    D, // down
    L, // left
    R, // right
};

const char upMove = 'U';
const char downMove = 'D';
const char leftMove = 'L';
const char rightMove = 'R';

struct EmptyNode {
    int xPosition;
    int yPosition;
};

class Node {
public:
    int cost;
    int boardWidth;
    int boardHeight;
    int **board;
    EmptyNode emptyNode;
    string movePath;

    Node(int height, int width) {
        boardHeight = height;
        boardWidth = width;
        cost = 0;
        movePath = "";

        board = new int*[boardHeight];
        int xPosition;
        int yPosition;
        for (xPosition = 0; xPosition < boardHeight; xPosition++) {
            board[xPosition] = new int[boardWidth];
            for (yPosition = 0; yPosition < boardWidth; yPosition++) {
                board[xPosition][yPosition] = (xPosition * boardWidth) + yPosition + 1;
            }
        }
        xPosition = boardHeight - 1;
        yPosition = boardWidth - 1;
        board[xPosition][yPosition] = 0;
        emptyNode.xPosition = xPosition;
        emptyNode.yPosition = yPosition;
    }

    Node(int height, int width, string initNode) {
        ConstructNodeFromString(height, width, initNode);
        cost = 0;
        movePath = "";
    }

    Node(int height, int width, string initNode, string path) {
        ConstructNodeFromString(height, width, initNode);
        movePath = path;
        cost = path.length();
    }

    Node(Node *node, directionType direction) {
        boardHeight = node->boardHeight;
        boardWidth = node->boardWidth;
        cost = node->cost + 1;

        board = new int*[boardHeight];
        int xPosition;
        int yPosition;
        for (xPosition = 0; xPosition < boardHeight; xPosition++) {
            board[xPosition] = new int[boardWidth];
            for (yPosition = 0; yPosition < boardWidth; yPosition++) {
                board[xPosition][yPosition] = node->board[xPosition][yPosition];
            }
        }

        xPosition = node->emptyNode.xPosition;
        yPosition = node->emptyNode.yPosition;
        string currentDirection;
        switch (direction) {
            case U:
                currentDirection = upMove;
                xPosition--;
                break;
            case D:
                currentDirection = downMove;
                xPosition++;
                break;
            case L:
                currentDirection = leftMove;
                yPosition--;
                break;
            case R:
                currentDirection = rightMove;
                yPosition++;
                break;
        }
        movePath = node->movePath + currentDirection;

        board[node->emptyNode.xPosition][node->emptyNode.yPosition] = board[xPosition][yPosition];
        board[xPosition][yPosition] = 0;
        emptyNode.xPosition = xPosition;
        emptyNode.yPosition = yPosition;
    }

    void ConstructNodeFromString(int height, int width, string initNode) {
        boardHeight = height;
        boardWidth = width;

        const string sep = SEPARATOR;

        int xPosition = 0;
        int yPosition = 0;
        board = new int*[boardHeight];
        board[xPosition] = new int[boardWidth];
        int pos = 0;
        string number;
        while (initNode.length() > 0) {
            pos = initNode.find(sep);
            number = initNode.substr(0, pos);
            initNode = initNode.substr(pos + 1);
            board[xPosition][yPosition] = StringToInt(number);

            if (StringToInt(number) == 0) { // TO-DO
                emptyNode.xPosition = xPosition;
                emptyNode.yPosition = yPosition;
            }

            if (yPosition >= (boardWidth - 1) && initNode.length() > 0) {
                xPosition++;
                yPosition = 0;
                board[xPosition] = new int[boardWidth];
            } else {
                yPosition++;
            }

        }
    }

    list<Node*> GetMoves() {
        list<Node*> moves;
        if (emptyNode.xPosition > 0) {
            moves.push_back(new Node(this, U));
        }
        if (emptyNode.xPosition < boardHeight - 1) {
            moves.push_back(new Node(this, D));
        }
        if (emptyNode.yPosition > 0) {
            moves.push_back(new Node(this, L));
        }
        if (emptyNode.yPosition < boardWidth - 1) {
            moves.push_back(new Node(this, R));
        }
        return moves;
    }

    int GetCost() {
        return cost;
    }

    void SetCost(int newCost) {
        cost = newCost;
    }

    string GetString() {
        string output = "";
        for (int xPosition = 0; xPosition < boardHeight; xPosition++) {
            for (int yPosition = 0; yPosition < boardWidth; yPosition++) {
                int num = board[xPosition][yPosition];
                output += IntToString(num) + SEPARATOR;
            }
        }
        return output;
    }

    void Print() {
        for (int xPosition = 0; xPosition < boardHeight; xPosition++) {
            for (int yPosition = 0; yPosition < boardWidth; yPosition++) {
                if (board[xPosition][yPosition] > 0) {
                    cout << setw(2) << board[xPosition][yPosition] << " ";
                } else {
                    cout << " * ";
                }
            }
            cout << endl;
        }
    }

    string IntToString(int number) {
        std::ostringstream oss;
        oss << number;
        return oss.str();
    }

    int StringToInt(string number) {
        const char *cstr = number.c_str();
        return atoi(cstr);
    }

    string GetMovePath() {
        return movePath;
    }

    void SetMovePath(string path) {
        movePath = path;
    }

    int GetWidth() {
        return boardWidth;
    }

    int GetHeight() {
        return boardHeight;
    }
};

class Solver {
public:
    int processNumber;
    Node *initNode;
    Node *startNode;
    Node *resultNode;
    Node *foundSolution;
    stack<Node*> solverStack;
    map<string, Node*> history;
    int moveLimit;
    int idle;

    Solver(int p) {
        processNumber = p;
        idle = 0;
    }

    void InitSolver(int width, int height, int cost, string initConfig) {
        initNode = new Node(height, width, initConfig);
        resultNode = new Node(height, width);
        moveLimit = cost;
        startNode = initNode;
        CleanUp();
    }

    void InitSolver(int width, int height, int cost, string initConfig, string prevMoves) {
        InitSolver(width, height, cost, initConfig);
        initNode->SetCost(prevMoves.length());
        initNode->SetMovePath(prevMoves);
        Node *node = initNode;

        while (prevMoves.length() != 0) {
            node = GetPrevNode(node, prevMoves);
            history.insert(pair<string, Node*>(node->GetString(), node));
            prevMoves = prevMoves.substr(0, prevMoves.length() - 1);
        }
        startNode = node;
    }

    void CleanUp() {
        foundSolution = NULL;
        stack<Node*> emptySolverStack;
        swap(solverStack, emptySolverStack);
        map<string, Node*> emptyHistory;
        swap(history, emptyHistory);
    }

    void PrintSolution(bool verbose) {
        if (foundSolution != NULL) {
            string movePath = foundSolution->GetMovePath();
            Node *node = startNode;
            cout << processNumber << ") Solution - " << foundSolution->GetCost() << " moves: " << foundSolution->GetMovePath() << endl;
            cout << processNumber << ") ------------------------------------------" << endl;
            if (verbose) {
                node->Print();

                while (movePath.length() != 0) {
                    node = GetNextNode(node, movePath);
                    movePath = movePath.substr(1);
                    cout << processNumber << ") ------------------------------------------" << endl;
                    node->Print();
                }
            }
        } else {
            cout << processNumber << ") Solution was not found..." << endl;
        }
    }

    Node *GetNextNode(Node *node, string movePath) {
        char nextMove = movePath.at(0);
        Node *nextNode = new Node(node, GetDirection(nextMove));
        return nextNode;
    }

    Node *GetPrevNode(Node *node, string movePath) {
        char prevMove = movePath.at(movePath.length() - 1);
        Node *prevNode = new Node(node, GetReverseDirection(prevMove));
        movePath = movePath.substr(0, movePath.length() - 1);
        prevNode->SetCost(movePath.length());
        prevNode->SetMovePath(movePath);

        return prevNode;
    }

    directionType GetDirection(char move) {
        directionType result;
        switch (move) {
            case upMove:
                result = U;
                break;
            case downMove:
                result = D;
                break;
            case leftMove:
                result = L;
                break;
            case rightMove:
                result = R;
                break;
        }
        return result;
    }

    directionType GetReverseDirection(char move) {
        directionType result;
        switch (move) {
            case upMove:
                result = D;
                break;
            case downMove:
                result = U;
                break;
            case leftMove:
                result = R;
                break;
            case rightMove:
                result = L;
                break;
        }
        return result;
    }

    void sendNewBestSolution() {
        int position = 0;
        char buffer[BUF_LENGTH];
        char nodeConfig[MSG_LENGTH];
        string nodeStr = foundSolution->GetString();
        strcpy(nodeConfig, nodeStr.c_str());
        int nodeConfigSize = strlen(nodeConfig) + 1;
        MPI_Request req;

        char nodeMoves[MOV_LENGTH];
        string nodePath = foundSolution->GetMovePath();
        strcpy(nodeMoves, nodePath.c_str());
        int nodeMovesSize = strlen(nodeMoves) + 1;

        MPI_Pack(&nodeConfigSize, 1, MPI_INT, buffer, BUF_LENGTH, &position, MPI_COMM_WORLD);
        MPI_Pack(&nodeMovesSize, 1, MPI_INT, buffer, BUF_LENGTH, &position, MPI_COMM_WORLD);
        MPI_Pack(nodeConfig, nodeConfigSize, MPI_CHAR, buffer, BUF_LENGTH, &position, MPI_COMM_WORLD);
        MPI_Pack(nodeMoves, nodeMovesSize, MPI_CHAR, buffer, BUF_LENGTH, &position, MPI_COMM_WORLD);
        // Send to master
        MPI_Isend(buffer, position, MPI_PACKED, 0, MSG_SOLUTION_SENT, MPI_COMM_WORLD, &req);
    }

    void receiveNewBestSolution() {
        MPI_Status status;
        int flag;

        MPI_Iprobe(MPI_ANY_SOURCE, MSG_SOLUTION_SENT, MPI_COMM_WORLD, &flag, &status);
        if (!flag) {
            return;
        }
        int position = 0;
        char buffer[BUF_LENGTH];
        int length = 0;
        char nodeConfig[MSG_LENGTH];
        char nodeMoves[MOV_LENGTH];
        int nodeConfigSize;
        int nodeMovesSize;
        Node* node;

        MPI_Recv(buffer, BUF_LENGTH, MPI_PACKED, MPI_ANY_SOURCE, MSG_SOLUTION_SENT, MPI_COMM_WORLD, &status);
        MPI_Unpack(buffer, BUF_LENGTH, &position, &nodeConfigSize, 1, MPI_INT, MPI_COMM_WORLD);
        MPI_Unpack(buffer, BUF_LENGTH, &position, &nodeMovesSize, 1, MPI_INT, MPI_COMM_WORLD);
        MPI_Unpack(buffer, BUF_LENGTH, &position, nodeConfig, nodeConfigSize, MPI_CHAR, MPI_COMM_WORLD);
        MPI_Get_count(&status, MPI_CHAR, &length);
        MPI_Unpack(buffer, BUF_LENGTH, &position, nodeMoves, nodeMovesSize, MPI_CHAR, MPI_COMM_WORLD);
        MPI_Get_count(&status, MPI_CHAR, &length);

        string nodeStr(nodeConfig);
        string nodePath(nodeMoves);

        cout << processNumber << ") Gained solution :-)" << endl;
        node = new Node(initNode->GetHeight(), initNode->GetWidth(), initNode->GetString());
        node->ConstructNodeFromString(this->initNode->boardHeight, this->initNode->boardWidth, nodeStr);
        node->movePath = nodePath;
        node->cost = nodePath.length();

        if (foundSolution == NULL) {
            this->foundSolution = node;
            this->moveLimit = node->GetCost();
        }
        if (foundSolution->GetCost() >= node->GetCost()) {
            cout << "Gained solution is better. Saving..." << endl;
            this->foundSolution = node;
            this->moveLimit = node->GetCost();
        }

    }

    void SendWork(Node *node, int destination) {
        int position = 0;
        char buffer[BUF_LENGTH];
        char nodeConfig[MSG_LENGTH];
        string nodeStr = node->GetString();
        strcpy(nodeConfig, nodeStr.c_str());
        int nodeConfigSize = strlen(nodeConfig) + 1;

        char nodeMoves[MOV_LENGTH];
        string nodePath = node->GetMovePath();
        strcpy(nodeMoves, nodePath.c_str());
        int nodeMovesSize = strlen(nodeMoves) + 1;

        MPI_Pack(&nodeConfigSize, 1, MPI_INT, buffer, BUF_LENGTH, &position, MPI_COMM_WORLD);
        MPI_Pack(&nodeMovesSize, 1, MPI_INT, buffer, BUF_LENGTH, &position, MPI_COMM_WORLD);
        MPI_Pack(nodeConfig, nodeConfigSize, MPI_CHAR, buffer, BUF_LENGTH, &position, MPI_COMM_WORLD);
        MPI_Pack(nodeMoves, nodeMovesSize, MPI_CHAR, buffer, BUF_LENGTH, &position, MPI_COMM_WORLD);
        /*
         * Buffer, number of process, mode, destination 
         */
        MPI_Send(buffer, position, MPI_PACKED, destination, MSG_WORK_SENT, MPI_COMM_WORLD);
    }

    void ReceiveWork() {
        int position = 0;
        char buffer[BUF_LENGTH];
        int length = 0;
        char nodeConfig[MSG_LENGTH];
        char nodeMoves[MOV_LENGTH];
        int nodeConfigSize;
        int nodeMovesSize;
        MPI_Status status;

        MPI_Recv(buffer, BUF_LENGTH, MPI_PACKED, MPI_ANY_SOURCE, MSG_WORK_SENT, MPI_COMM_WORLD, &status);
        MPI_Unpack(buffer, BUF_LENGTH, &position, &nodeConfigSize, 1, MPI_INT, MPI_COMM_WORLD);
        MPI_Unpack(buffer, BUF_LENGTH, &position, &nodeMovesSize, 1, MPI_INT, MPI_COMM_WORLD);
        MPI_Unpack(buffer, BUF_LENGTH, &position, nodeConfig, nodeConfigSize, MPI_CHAR, MPI_COMM_WORLD);
        MPI_Get_count(&status, MPI_CHAR, &length);
        MPI_Unpack(buffer, BUF_LENGTH, &position, nodeMoves, nodeMovesSize, MPI_CHAR, MPI_COMM_WORLD);
        MPI_Get_count(&status, MPI_CHAR, &length);

        string nodeStr(nodeConfig);
        string nodePath(nodeMoves);

        cout << processNumber << ") Node config received: " << nodeConfig << endl;
        cout << processNumber << ") Prev moves received: " << nodeMoves << endl;
        this->initNode->cost = nodePath.length();
        this->initNode->movePath = nodeMoves;
        this->initNode->ConstructNodeFromString(this->initNode->boardHeight, this->initNode->boardWidth, nodeStr);
        moveLimit -= initNode->GetCost();
    }

    bool askForWork(int destination) {
        int position = 0;
        char buffer[BUF_LENGTH];
        int length = 0;
        char nodeConfig[MSG_LENGTH];
        char nodeMoves[MOV_LENGTH];
        int nodeConfigSize;
        int nodeMovesSize;
        MPI_Status status;

        //Send ask for work 
        MPI_Send(buffer, position, MPI_PACKED, destination, MSG_WORK_ASK, MPI_COMM_WORLD);

        MPI_Recv(buffer, BUF_LENGTH, MPI_PACKED, destination, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if (status.MPI_TAG != MSG_WORK_SENT) {
            return false;
        }

        MPI_Unpack(buffer, BUF_LENGTH, &position, &nodeConfigSize, 1, MPI_INT, MPI_COMM_WORLD);
        MPI_Unpack(buffer, BUF_LENGTH, &position, &nodeMovesSize, 1, MPI_INT, MPI_COMM_WORLD);
        MPI_Unpack(buffer, BUF_LENGTH, &position, nodeConfig, nodeConfigSize, MPI_CHAR, MPI_COMM_WORLD);
        MPI_Get_count(&status, MPI_CHAR, &length);
        MPI_Unpack(buffer, BUF_LENGTH, &position, nodeMoves, nodeMovesSize, MPI_CHAR, MPI_COMM_WORLD);
        MPI_Get_count(&status, MPI_CHAR, &length);

        string nodeStr(nodeConfig);
        string nodePath(nodeMoves);

        cout << processNumber << ") Node config received: " << nodeConfig << endl;
        cout << processNumber << ") Prev moves received: " << nodeMoves << endl;
        this->initNode->cost = nodePath.length();
        this->initNode->movePath = nodeMoves;
        this->initNode->ConstructNodeFromString(this->initNode->boardHeight, this->initNode->boardWidth, nodeStr);
        moveLimit -= initNode->GetCost();
        return true;
    }

    void Solve(bool master) {
        Node *node;
        list<Node*> nodeMoves;
        solverStack.push(initNode);

        if (master) {
            cout << processNumber << ") Master" << endl;
            int sizeP = 0;
            MPI_Comm_size(MPI_COMM_WORLD, &sizeP);

            history.insert(pair<string, Node*>(initNode->GetString(), initNode));
            while (1) {
                node = solverStack.top();
                solverStack.pop();
                /* We must check, if found solution is cheapest */
                if (AreNodesSame(*node, *resultNode)) {
                    cout << processNumber << ") Found soluton: " << node->GetMovePath() << endl;
                    if (node->GetCost() <= moveLimit) {
                        foundSolution = node;
                        moveLimit = node->GetCost();
                    }
                    continue;
                }

                nodeMoves = node->GetMoves();
                list<Node*>::iterator iterNextMove;
                for (iterNextMove = nodeMoves.begin(); iterNextMove != nodeMoves.end(); iterNextMove++) {
                    Node *nextMove = *iterNextMove;

                    map<string, Node*>::iterator iterHistory;
                    string nodeKey = nextMove->GetString();
                    iterHistory = history.find(nodeKey);
                    // historia zabezpeci, ze sa nezacykli vyhladavanie
                    if (iterHistory != history.end()) { // ak sa nachadza v historii
                        if (iterHistory->second->GetCost() > nextMove->GetCost()) {
                            iterHistory->second->SetCost(nextMove->GetCost());
                            solverStack.push(nextMove);
                        }
                    } else {
                        if (nextMove->GetCost() <= moveLimit) {
                            solverStack.push(nextMove);
                            history.insert(pair<string, Node*>(nextMove->GetString(), nextMove));
                        }
                    }
                } // Foreach end
                cout << processNumber << ") Size of stack: " << solverStack.size() << endl;
                // Dummy divide work
                if (solverStack.size() > sizeP) {
                    break;
                }
            }

            for (int index = 1; index < sizeP; index++) {
                node = solverStack.top();
                solverStack.pop();
                cout << processNumber << ") Sending work: " << node->GetString() << endl;
                this->SendWork(node, index);
            }
        } else {
            this->ReceiveWork();
            cout << processNumber << ") Starting solution with cost:" <<
                    initNode->GetCost() << " and movelimit: " << moveLimit << endl;
        }

        while (!solverStack.empty()) {
            if (master) {
                this->receiveNewBestSolution();
            }
            this->replyAskForWork(false);
            node = solverStack.top();
            solverStack.pop();
            /* We must check, if found solution is cheapest */
            if (AreNodesSame(*node, *resultNode)) {
                cout << processNumber << ") Found soluton: " << node->GetMovePath() << endl;
                if (node->GetCost() <= moveLimit) {
                    foundSolution = node;
                    moveLimit = node->GetCost();
                }
                continue;
            }

            nodeMoves = node->GetMoves();
            list<Node*>::iterator iterNextMove;
            for (iterNextMove = nodeMoves.begin(); iterNextMove != nodeMoves.end(); iterNextMove++) {
                Node *nextMove = *iterNextMove;

                map<string, Node*>::iterator iterHistory;
                string nodeKey = nextMove->GetString();
                iterHistory = history.find(nodeKey);
                // historia zabezpeci, ze sa nezacykli vyhladavanie
                if (iterHistory != history.end()) { // ak sa nachadza v historii
                    if (iterHistory->second->GetCost() > nextMove->GetCost()) {
                        iterHistory->second->SetCost(nextMove->GetCost());
                        solverStack.push(nextMove);
                    }
                } else {
                    if (nextMove->GetCost() <= moveLimit) {
                        solverStack.push(nextMove);
                        history.insert(pair<string, Node*>(nextMove->GetString(), nextMove));
                    }
                }

            }
        }
        this->idle = 1;
    }

    bool getWorkOrExit(int max) {
        int destination = (int) floor(rand() / (RAND_MAX + 1.0) * (max + 1));
        if (this->askForWork(destination)) {
            this->idle = 0;
            return true;
        }
    }

    bool sendEndFlag(int count) {
        int position = 0;
        char buffer[BUF_LENGTH];
        MPI_Status status;
        int flag;
        int receive_from, destination;

        //Send ask for work 
        if (this->idle) {
            flag = MSG_IS_END;
        } else {
            flag = MSG_IS_NOT_END;
        }

        destination = (this->processNumber - 1) % count;
        MPI_Send(buffer, position, MPI_PACKED, destination, flag, MPI_COMM_WORLD);

        receive_from = count - this->processNumber - 1;
        MPI_Recv(buffer, BUF_LENGTH, MPI_PACKED, receive_from, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        if (status.MPI_TAG != MSG_IS_END) {
            return false;
        }
        return true;
    }

    bool allEnded(int count) {
        return this->sendEndFlag(count);
    }

    void receiveEnd(int count) {
        int position = 0;
        char buffer[BUF_LENGTH];
        MPI_Status status;
        int flag;
        int receive_from, destination;

        //Send ask for work 
        if (this->idle) {
            flag = MSG_IS_END;
        } else {
            flag = MSG_IS_NOT_END;
        }

        receive_from = count - this->processNumber - 1;
        MPI_Recv(buffer, BUF_LENGTH, MPI_PACKED, receive_from, MPI_ANY_TAG, MPI_COMM_WORLD, &status);

        if (status.MPI_TAG != MSG_IS_END) {
            flag = MSG_IS_NOT_END;
        }
        
        destination = (this->processNumber - 1) % count;
        MPI_Send(buffer, position, MPI_PACKED, destination, flag, MPI_COMM_WORLD);
        
    }
    
    void replyAskForWork(bool ended) {
        MPI_Status status;
        int flag;

        MPI_Iprobe(MPI_ANY_SOURCE, MSG_WORK_ASK, MPI_COMM_WORLD, &flag, &status);
        if (!flag) {
            return;
        }
        
        int position = 0;
        char buffer[BUF_LENGTH];

        if (ended) {
            MPI_Send(buffer, position, MPI_PACKED, status.MPI_SOURCE, MSG_IS_END, MPI_COMM_WORLD);
        } else {
            Node* node = solverStack.top();
            solverStack.pop();
            this->SendWork(node, status.MPI_SOURCE);
        }
    }

    void waitForAllEnded(int count) {
        int i = 0;
        while (1) {
            // beware of cycle
            i++;
            if (i > 10000) {
                cout << "Loop" <<endl;
                break;
            }
            this->receiveNewBestSolution();
            this->replyAskForWork(false);
            if (this->allEnded(count)) {
                return;
            }
        }
    }

    bool AreNodesSame(Node& firstNode, Node& secondNode) {
        bool areSame = false;
        if (firstNode.boardHeight == secondNode.boardHeight &&
                firstNode.boardWidth == secondNode.boardWidth) {
            areSame = (firstNode.GetString() == secondNode.GetString());
        }
        return areSame;
    }

};

int main(int argc, char** argv) {
    int p;
    int count = 1;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &p);
    MPI_Comm_size(MPI_COMM_WORLD, &count);

    Solver *solver = new Solver(p);
    solver->InitSolver(3, 3, 20, "1,2,3,0,7,5,8,4,6,");

    if (p == 0) {
        solver->Solve(true);
        solver->waitForAllEnded(count);
    } else {
        do {
            solver->Solve(false);
            solver->sendNewBestSolution();
            solver->replyAskForWork(false);
            solver->receiveEnd(count);
            do {} while (!solver->getWorkOrExit(count));
        } while (!solver->idle);
    }

    solver->PrintSolution(false);
    MPI_Finalize();


    /*
    solver->InitSolver(2, 2, 20, "0,2,1,3,");
    solver->Solve();
    solver->PrintSolution();

    solver->InitSolver(2, 2, 20, "1,2,3,0,", "DR");
    solver->Solve();
    solver->PrintSolution();

    solver->InitSolver(3, 3, 20, "1,2,3,7,4,5,8,0,6,", "RD");
    solver->Solve();
    solver->PrintSolution();
     */
    return 0;
}
